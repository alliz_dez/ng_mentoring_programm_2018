import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { LoginPageComponent } from './login-page/login-page.component';
import { LogoHalfComponent } from './logo-half/logo-half.component';
import { AuthHalfComponent } from './auth-half/auth-half.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    LoginPageComponent,
    LogoHalfComponent,
    AuthHalfComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  exports: [
    LoginPageComponent,
    LogoHalfComponent,
    AuthHalfComponent
  ]
})
export class AuthModule { }
