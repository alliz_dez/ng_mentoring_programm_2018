import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoHalfComponent } from './logo-half.component';

describe('LogoHalfComponent', () => {
  let component: LogoHalfComponent;
  let fixture: ComponentFixture<LogoHalfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoHalfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoHalfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
