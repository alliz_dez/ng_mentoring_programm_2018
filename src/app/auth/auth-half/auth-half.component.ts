import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-auth-half',
  templateUrl: './auth-half.component.html',
  styleUrls: ['./auth-half.component.css']
})
export class AuthHalfComponent implements OnInit {
  @Output() handleLogin = new EventEmitter();
  loginForm = this.fb.group({
    username: [''],
    password: [''],
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
  }

  handleLoginClick() {
    this.handleLogin.emit(this.loginForm.value);
  }

}
