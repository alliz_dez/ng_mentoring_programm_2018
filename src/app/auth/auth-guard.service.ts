import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {
  private isLoggedIn$: BehaviorSubject<boolean>;

  constructor(private router: Router, private authService: AuthService) {
    this.authService.isAuthenticated().subscribe(loggedIn => this.isLoggedIn$ = new BehaviorSubject(loggedIn));
  }

  canActivate(): Observable<boolean> {
    const isLoggedIn$ = this.isLoggedIn$.asObservable();
    if (!isLoggedIn$) {
      this.router.navigate(['auth']);
    }
    return isLoggedIn$;
  }

}
