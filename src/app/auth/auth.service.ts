import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';

import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly logger$: BehaviorSubject<boolean>;

  constructor(private apiService: ApiService, private router: Router) {
    this.logger$ = new BehaviorSubject(!!this.getToken());
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  isAuthenticated(): Observable<boolean> {
    return this.logger$.asObservable();
  }

  login(login: string, password: string) {
    this.apiService.post('auth/login', { login, password })
      .subscribe((data: { token: string }) => {
        localStorage.setItem('token', data.token);
        this.setLogger();
        this.router.navigate(['/courses']);
      });
  }

  logout() {
    localStorage.removeItem('token');
    this.setLogger();
    this.router.navigate(['/auth']);
  }

  private setLogger() {
    this.logger$.next(!!this.getToken());
  }
}
