import { Route } from '@angular/router';
import { RootComponent } from './core/root/root.component';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { CoursesListComponent } from './courses-list/courses-list/courses-list.component';
import { NoContentPageComponent } from './core/no-content-page/no-content-page.component';
import { NewCoursePageComponent } from './course-page/new-course-page/new-course-page.component';
import { EditCoursePageComponent } from './course-page/edit-course-page/edit-course-page.component';

import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';

export const ROUTES: Route[] = [
  {
    path: '',
    component: RootComponent,
    children: [
      { path: '', redirectTo: 'courses', pathMatch: 'full' },

      { path: 'auth', component: LoginPageComponent },
      { path: 'courses', component: CoursesListComponent, canActivate: [AuthGuard]  },
      { path: 'courses/new', component: NewCoursePageComponent, data: { breadcrumb: 'new course', canActivate: [AuthGuard] } },
      { path: 'courses/:id', component: EditCoursePageComponent, data: { breadcrumb: 'course', canActivate: [AuthGuard] } },

      { path: '**', component: NoContentPageComponent }
    ]
  },
];
