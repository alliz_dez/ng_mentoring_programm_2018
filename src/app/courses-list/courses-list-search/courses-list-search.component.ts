import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';

import { CourseService } from '../../shared/course.service';

@Component({
  selector: 'app-courses-list-search',
  templateUrl: './courses-list-search.component.html',
  styleUrls: ['./courses-list-search.component.css']
})
export class CoursesListSearchComponent implements OnInit {
  @ViewChild('search') search: ElementRef;

  constructor(private courseService: CourseService) { }

  ngOnInit() {
    fromEvent(this.search.nativeElement, 'keyup')
      .pipe(
        map((event: any) => event.target.value),
        filter((value) => value.length >= 3),
        debounceTime(300),
        distinctUntilChanged(),
      )
      .subscribe((value: string) => this.courseService.searchCourse(value));
  }
}
