import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { CoursesListSearchComponent } from './courses-list-search.component';

describe('CoursesListSearchComponent', () => {
  let component: CoursesListSearchComponent;
  let fixture: ComponentFixture<CoursesListSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [CoursesListSearchComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesListSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Search input', () => {
    it('should keep input and inputValue in sync', () => {
      const inputDe = fixture.debugElement.query(By.css('input'));
      const inputEl = inputDe.nativeElement;
      inputEl.value = 'Updated value';
      inputEl.dispatchEvent(new Event('input'));
      expect(component.inputValue).toBe('Updated value');
    });
  });

  describe('Search button', () => {
    it('should console log inputValue', () => {
      console.log = jasmine.createSpy('log');
      component.inputValue = 'loggy';
      const buttonDe = fixture.debugElement.query(By.css('.search_button_search'));
      buttonDe.triggerEventHandler('click', null);
      expect(console.log).toHaveBeenCalledWith('loggy');
    });
  });
});
