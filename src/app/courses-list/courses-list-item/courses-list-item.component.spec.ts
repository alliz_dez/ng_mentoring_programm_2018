import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';

import { ICourseItem } from '../../i-course-item.model';
import { CoursesListItemComponent } from './courses-list-item.component';

@Component({
  template: `
    <app-courses-list-item [courseItem]="courseItem" (deleteItem)="onDeleteItem($event)">
    </app-courses-list-item>`
})
class TestHostComponent {
  courseItem: ICourseItem = {
    id: 0,
    name: 'title 1',
    date: new Date(2018, 0, 32),
    duration: 1,
    description: 'description 1',
    isTopRated: false
  };
  onDeleteItem(id: number) { console.log(id); }
}

describe('CoursesListItemComponent', () => {
  let testHost: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CoursesListItemComponent, TestHostComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHost = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(testHost).toBeTruthy();
  });

  it('should raise deleteItem', () => {
    console.log = jasmine.createSpy('log');
    const buttonDe = fixture.debugElement.query(By.css('.course_button_delete'));
    buttonDe.triggerEventHandler('click', null);
    expect(console.log).toHaveBeenCalledWith(0);
  });
});
