import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { ICourseItem } from '../../i-course-item.model';

@Component({
  selector: 'app-courses-list-item',
  templateUrl: './courses-list-item.component.html',
  styleUrls: ['./courses-list-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoursesListItemComponent implements OnInit {
  @Input() courseItem: ICourseItem;
  @Output() deleteItem = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.deleteItem.emit(this.courseItem.id);
  }
}
