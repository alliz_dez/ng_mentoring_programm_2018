import { Component, OnInit } from '@angular/core';

import { ICourseItem } from '../../i-course-item.model';
import { SortByNamePipe } from '../sort-by-name.pipe';
import { CourseService } from '../../shared/course.service';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.css']
})
export class CoursesListComponent implements OnInit {
  public isDeleteConfirmation = false;
  public courses: ICourseItem[] = [];

  private idForDelete: number;

  constructor(private sortByNamePipe: SortByNamePipe, private courseService: CourseService) { }

  ngOnInit() {
    this.courseService.getCoursesList().subscribe(coursesList => this.courses = coursesList);
  }

  loadMore() {
    this.courseService.fetchCoursesList();
  }

  toggleDelete(id) {
    this.idForDelete = id;
    this.isDeleteConfirmation = !this.isDeleteConfirmation;
  }

  handleDelete() {
    this.courseService.removeCourse(this.idForDelete);
    this.isDeleteConfirmation = false;
  }
}
