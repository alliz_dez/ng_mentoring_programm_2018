import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortByName'
})
export class SortByNamePipe implements PipeTransform {

  transform(coursesList: any[], name: string): any {
    if (!coursesList || !name) {
      return coursesList;
    }
    return coursesList.filter(course => course.name === name);
  }

}
