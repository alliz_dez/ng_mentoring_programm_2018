import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appCourseBorder]'
})
export class CourseBorderDirective implements OnInit {
  @Input('appCourseBorder') creationDate: Date;
  private currentDate = new Date();

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
    this.setBorder();
  }

  private setBorder(): void {
    const blue = '69, 92, 116';
    const green = '4, 164, 160';
    if (this.creationDate < this.currentDate && this.creationDate >= this.getDateTwoWeekAgo()) {
      this.setBoxShadowStyle(green);
    } else if (this.creationDate > this.currentDate) {
      this.setBoxShadowStyle(blue);
    }
  }

  private getDateTwoWeekAgo(): Date {
    return new Date(this.currentDate.setDate(this.currentDate.getDate() - 14));
  }

  private setBoxShadowStyle(color): void {
    this.renderer.setStyle(this.el.nativeElement, 'box-shadow', `0 0 6px 0 rgba(${color}, 1)`);
  }
}
