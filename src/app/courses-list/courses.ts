import { ICourseItem } from '../i-course-item.model';

export const COURSES: ICourseItem[] = [
  {
    id: 0,
    name: 'title 1',
    date: new Date(2018, 0, 32),
    duration: 1,
    description: 'description 1',
    isTopRated: true
  },
  {
    id: 1,
    name: 'title 2',
    date: new Date(2018, 0, 2),
    duration: 30,
    description: 'description 2',
    isTopRated: false
  },
  {
    id: 2,
    name: 'title 3',
    date: new Date(2019, 7, 3),
    duration: 74,
    description: 'description 3',
    isTopRated: false
  },
  {
    id: 3,
    name: 'title 4',
    date: new Date(2018, 7, 12),
    duration: 60,
    description: 'description 4',
    isTopRated: true
  }
];
