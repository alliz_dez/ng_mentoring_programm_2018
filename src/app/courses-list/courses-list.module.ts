import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CoursesListComponent } from './courses-list/courses-list.component';
import { CoursesListSearchComponent } from './courses-list-search/courses-list-search.component';
import { CoursesListItemComponent } from './courses-list-item/courses-list-item.component';
import { SharedModule } from '../shared/shared.module';
import { CourseBorderDirective } from './course-border.directive';
import { SortByNamePipe } from './sort-by-name.pipe';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule
  ],
  declarations: [
    CoursesListComponent,
    CoursesListSearchComponent,
    CoursesListItemComponent,
    CourseBorderDirective,
    SortByNamePipe
  ],
  providers: [SortByNamePipe]
})
export class CoursesListModule { }
