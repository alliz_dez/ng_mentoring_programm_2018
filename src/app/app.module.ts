import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { ROUTES } from './app.routes';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { TokenInterceptor } from './auth/token-interceptor';
import { LoaderInterceptor } from './core/loader/loader-interceptor';
import { CoursesListModule } from './courses-list/courses-list.module';
import { SharedModule } from './shared/shared.module';
import { CoursePageModule } from './course-page/course-page.module';

@NgModule({
  bootstrap: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    }
  ],
  declarations: [ AppComponent ],
  imports: [
    BrowserModule,
    CoreModule,
    HttpClientModule,
    SharedModule,
    AuthModule,
    CoursesListModule,
    CoursePageModule,
    RouterModule.forRoot(ROUTES, { useHash: true })
  ]
})
export class AppModule {
}
