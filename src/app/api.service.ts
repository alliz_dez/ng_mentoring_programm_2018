import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

interface PgPapams {
  start: number;
  count: number;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseUrl = 'http://localhost:3004';

  constructor(private http: HttpClient) { }

  private concatUrl(url: string): string {
    return `${this.baseUrl}/${url}`;
  }

  get(url: string): Observable<Object> {
    return this.http.get(this.concatUrl(url));
  }

  getPg(url: string, { start, count }: PgPapams) {
    return this.get(`${url}?start=${start}&count=${count}`);
  }

  getSearch(url: string, word: string) {
    return this.http.get(`${this.concatUrl(url)}?q=${word}`);
  }

  put(url: string, item: any) {
    return this.http.put(url, item);
  }

  post(url: string, body?: Object): Observable<Object> {
    return this.http.post(this.concatUrl(url), body);
  }

  delete(url: string, id: number) {
    return this.http.delete(`${this.concatUrl(url)}/${id}`);
  }

}
