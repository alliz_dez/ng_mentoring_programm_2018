import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { ICourseItem } from '../../i-course-item.model';

const emptyCourse: ICourseItem = {
  id: null,
  name: '',
  date: new Date(),
  description: '',
  isTopRated: false,
  duration: null
};

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: [ './course-form.component.css' ],
})
export class CourseFormComponent implements OnInit {
  @Input() courseItem: ICourseItem;
  @Output() save = new EventEmitter<ICourseItem>();

  private currentCourse: ICourseItem;
  courseForm: FormGroup;

  private static transformFieldsToFormFields(fields) {
    return Object.entries(fields).reduce((result, [key, value]) => {
      return {...result, [key]: [value] };
    }, {});
  }

  constructor(private fb: FormBuilder, private router: Router) {
  }

  ngOnInit() {
    this.currentCourse = this.courseItem || emptyCourse;
    const formFields = CourseFormComponent.transformFieldsToFormFields(this.currentCourse);

    this.courseForm = this.fb.group(formFields);
  }

  handleSave() {
    this.save.emit(this.courseForm.value);
  }

  handleCancel() {
    this.router.navigate(['/courses']);
  }

}
