import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ICourseItem } from '../../i-course-item.model';
import { CourseService } from '../../shared/course.service';

@Component({
  selector: 'app-edit-course-page',
  templateUrl: './edit-course-page.component.html',
  styleUrls: ['./edit-course-page.component.css']
})
export class EditCoursePageComponent implements OnInit {
  private courseItem: ICourseItem;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    let id: number;

    this.route.params.subscribe(data => id = Number(data.id));
    this.courseService.getCourseById(id).subscribe((course: ICourseItem) => this.courseItem = course);
  }

  handleSave(course) {
    this.courseService.updateCourse(course);
    this.router.navigate(['/courses']);
  }

}
