import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CourseService } from '../../shared/course.service';

@Component({
  selector: 'app-new-course-page',
  templateUrl: './new-course-page.component.html',
  styleUrls: ['./new-course-page.component.css']
})
export class NewCoursePageComponent implements OnInit {

  constructor(private router: Router, private courseService: CourseService) { }

  ngOnInit() {
  }

  handleSave(course) {
    console.log(course)
    this.courseService.createCourse(course);
    this.router.navigate(['/courses']);
  }

}
