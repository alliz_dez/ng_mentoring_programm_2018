import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CourseFormComponent } from './course-form/course-form.component';
import { SharedModule } from '../shared/shared.module';
import { NewCoursePageComponent } from './new-course-page/new-course-page.component';
import { EditCoursePageComponent } from './edit-course-page/edit-course-page.component';

@NgModule({
  declarations: [
    CourseFormComponent,
    NewCoursePageComponent,
    EditCoursePageComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
  ]
})
export class CoursePageModule { }
