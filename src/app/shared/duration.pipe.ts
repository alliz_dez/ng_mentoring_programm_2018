import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(duration): any {
    const h = Math.round(duration / 60);
    const modulo = duration % 60;

    if (duration <= 1) {
      return '< min';
    } else if (duration < 60) {
      return `${duration}min`;
    } else if (!(modulo)) {
      return `${h}h`;
    }
    return `${h}h ${modulo}min`;
  }

}
