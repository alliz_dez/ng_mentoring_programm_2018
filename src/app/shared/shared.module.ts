import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderByPipe } from './order-by.pipe';
import { DurationPipe } from './duration.pipe';
import { LogoComponent } from './logo/logo.component';
import { WebsiteNameComponent } from './website-name/website-name.component';
import { NoDataComponent } from './no-data/no-data.component';
import { ConfirmDeleteModalComponent } from './confirm-delete-modal/confirm-delete-modal.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    LogoComponent,
    WebsiteNameComponent,
    NoDataComponent,
    DurationPipe,
    OrderByPipe,
    ConfirmDeleteModalComponent,
  ],
  exports: [
    LogoComponent,
    WebsiteNameComponent,
    NoDataComponent,
    ConfirmDeleteModalComponent,
    DurationPipe,
    OrderByPipe,
  ]
})
export class SharedModule { }
