import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { ICourseItem } from '../i-course-item.model';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private page = 0;
  private coursesList$ = new BehaviorSubject<ICourseItem[]>([]);

  constructor(private apiService: ApiService) {
    this.get().subscribe((courses: ICourseItem[]) => this.next(courses));
  }

  private get() {
    return this.apiService.getPg('courses', { start: this.page, count: 10 });
  }

  private next(list: ICourseItem[]) {
    this.coursesList$.next(list);
  }

  private onSuccess(res) {
    if (res) {
      this.next([...this.coursesList$.value, ...res]);
    }
  }

  getCoursesList() {
    return this.coursesList$.asObservable();
  }

  fetchCoursesList() {
    this.page = this.page + 1;
    this.get().subscribe((courses: ICourseItem[]) => this.onSuccess(courses));
  }

  searchCourse(word: string) {
    this.apiService.getSearch('courses', word)
      .subscribe((courses: ICourseItem[]) => this.next(courses));
  }

  getCourseById(id): Observable<Object> {
    return this.apiService.get(`courses/${id}`);
  }

  createCourse(course) {
    this.apiService.post('courses', course)
      .subscribe((newCourse: ICourseItem) => this.next([...this.coursesList$.value, newCourse]));
  }

  updateCourse(course) {
    this.next(this.coursesList$.value.map(item => item.id === course.id ? course : item));
  }

  removeCourse(id) {
    this.apiService.delete('courses', id)
      .subscribe(() => this.next(this.coursesList$.value.filter(item => item.id !== id)));
  }

}
