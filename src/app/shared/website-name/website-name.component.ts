import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-website-name',
  templateUrl: './website-name.component.html',
  styleUrls: ['./website-name.component.css']
})
export class WebsiteNameComponent implements OnInit {
  @Input() size: string;

  constructor() { }

  ngOnInit() {
  }

}
