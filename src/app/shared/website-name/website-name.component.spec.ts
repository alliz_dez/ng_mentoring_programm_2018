import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsiteNameComponent } from './website-name.component';

describe('WebsiteNameComponent', () => {
  let component: WebsiteNameComponent;
  let fixture: ComponentFixture<WebsiteNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebsiteNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsiteNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
