import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private isLoggedIn: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.setAuthInfo();
  }

  setAuthInfo() {
    this.authService.isAuthenticated().subscribe((loggedIn: boolean) => this.isLoggedIn = loggedIn);
  }

  handleLogout() {
    this.authService.logout();
  }
}
