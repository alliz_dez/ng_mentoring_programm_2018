import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { RootComponent } from './root/root.component';
import { SharedModule } from '../shared/shared.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { NoContentPageComponent } from './no-content-page/no-content-page.component';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule],
  declarations: [
    HeaderComponent,
    FooterComponent,
    BreadcrumbComponent,
    NoContentPageComponent,
    RootComponent,
    LoaderComponent
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    BreadcrumbComponent,
    LoaderComponent
  ]
})
export class CoreModule {
}
