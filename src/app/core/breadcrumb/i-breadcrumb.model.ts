import { Params } from '@angular/router';

export interface IBreadcrumb {
  params?: Params;
  label: string;
  url: string;
}
