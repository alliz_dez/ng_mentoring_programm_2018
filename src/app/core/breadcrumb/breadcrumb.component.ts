import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { IBreadcrumb } from './i-breadcrumb.model';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent {
  breadcrumbs = this.router.events.pipe(
    filter(event => event instanceof NavigationEnd),
    distinctUntilChanged(),
    map(event => this.buildBreadcrumbs(this.activatedRoute.root))
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  private getLabel(routeConfig, params) {
    return routeConfig && routeConfig.data
      ? `${routeConfig.data.breadcrumb} ${params.id || ''}`
      : 'Courses';
  }

  private isBreadcrumbUnique(breadcrumbs, label) {
    return breadcrumbs.some(item => item.label === label);
  }

  private generateNewBreadCrumbs(routeConfig, breadcrumbs, breadcrumb) {
    return routeConfig && !this.isBreadcrumbUnique(breadcrumbs, breadcrumb.label)
      ? [...breadcrumbs, breadcrumb]
      : breadcrumbs;
  }

  private buildBreadcrumbs(route: ActivatedRoute, breadcrumbs: IBreadcrumb[] = []): IBreadcrumb[] {
    const { routeConfig, snapshot: { url, params } } = route;
    const label = this.getLabel(routeConfig, params);
    const path = url.length ? `${url[0]}/${url[1]}` : '';

    const breadcrumb = { label: label, url: path };

    const newBreadcrumbs = this.generateNewBreadCrumbs(routeConfig, breadcrumbs, breadcrumb);

    if (route.firstChild) {
      return this.buildBreadcrumbs(route.firstChild, newBreadcrumbs);
    }
    return newBreadcrumbs;
  }

}
