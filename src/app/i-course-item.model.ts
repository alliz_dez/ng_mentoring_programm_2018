export interface ICourseItem {
  id?: number;
  duration?: number | null;
  name: string;
  date: Date;
  description: string;
  isTopRated: boolean;
}
